package ru.itpark.service;

import org.springframework.web.multipart.MultipartFile;
import ru.itpark.entity.Balance;
import ru.itpark.entity.Purchase;

import javax.transaction.Transactional;

public interface BaseService {
    @Transactional
    void savePurchase(Purchase purchase, MultipartFile image);

    @Transactional
    void updateBalance(int id, Balance balance);

    @Transactional
    void removePurchase(int id);
}
