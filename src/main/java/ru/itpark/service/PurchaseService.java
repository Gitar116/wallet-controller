package ru.itpark.service;

import org.springframework.web.multipart.MultipartFile;
import ru.itpark.entity.Purchase;

import java.util.List;

public interface PurchaseService {
    List<Purchase> getAll();

    void add(Purchase purchase, MultipartFile image);

    Purchase findById(int id);

    void removeById(int id);
}
