package ru.itpark.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itpark.entity.Balance;
import ru.itpark.entity.Purchase;

import javax.transaction.Transactional;

@Service
public class BaseServiceImpl implements BaseService {
    private PurchaseService purchaseService;
    private BalanceService balanceService;

    public BaseServiceImpl(PurchaseService purchaseService, BalanceService balanceService) {
        this.purchaseService = purchaseService;
        this.balanceService = balanceService;
    }

    @Override
    @Transactional
    public void savePurchase(Purchase purchase, MultipartFile image) {
        purchaseService.add(purchase, image);
        balanceService.sellFrom(purchase.getBalance().getId(), purchase.getPrice());
    }

    @Override
    @Transactional
    public void updateBalance(int id, Balance balance){
        balanceService.update(id, balance.getBalance());
    }

    @Override
    @Transactional
    public void removePurchase(int id){
        balanceService.removePurch(purchaseService.findById(id).getBalance().getId(),purchaseService.findById(id).getPrice() );
        purchaseService.removeById(id);
    }
}
