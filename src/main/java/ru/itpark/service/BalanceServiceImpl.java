package ru.itpark.service;

import org.springframework.stereotype.Service;
import ru.itpark.entity.Balance;
import ru.itpark.exception.BalanceNotFoundException;
import ru.itpark.repository.BalanceRepository;

import java.util.List;

@Service
public class BalanceServiceImpl implements BalanceService{
    private final BalanceRepository balanceRepository;

    public BalanceServiceImpl(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    @Override
    public Balance findById(int id) {
        return balanceRepository.getOne(id);
    }

    @Override
    public List<Balance> getAll() {
        return balanceRepository.findAll();
    }

    @Override
    public void update(int balanceId, int money) {
        Balance balance = balanceRepository.findById(balanceId).orElseThrow(BalanceNotFoundException::new);
        balance.setBalance(money);
        balanceRepository.save(balance);
    }

    @Override
    public void sellFrom(int balanceId, int price) {
        Balance balance = balanceRepository.findById(balanceId).orElseThrow(BalanceNotFoundException::new);
        balance.setBalance(balance.getBalance() - price);
        balanceRepository.save(balance);
    }

    @Override
    public void removePurch(int balanceId, int price){
        Balance balance = balanceRepository.findById(balanceId).orElseThrow(BalanceNotFoundException::new);
        balance.setBalance(balance.getBalance() + price);
        balanceRepository.save(balance);
    }

    @Override
    public void saveBalance(Balance balance) {
        balanceRepository.save(balance);
    }

    @Override
    public void removeBalance(int id) {
        balanceRepository.deleteById(id);
    }


}
