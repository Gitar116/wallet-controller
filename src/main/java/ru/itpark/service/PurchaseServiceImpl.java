package ru.itpark.service;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itpark.entity.Purchase;
import ru.itpark.repository.PurchaseRepository;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class PurchaseServiceImpl implements PurchaseService {
    private final PurchaseRepository purchaseRepository;
    private final Environment environment;

    public PurchaseServiceImpl(PurchaseRepository purchaseRepository, Environment environment) {
        this.purchaseRepository = purchaseRepository;
        this.environment = environment;
    }

    @Override
    public List<Purchase> getAll() {
        return purchaseRepository.findAll();
    }

    @Override
    public void add(Purchase purchase, MultipartFile image) {
        Purchase saved = purchaseRepository.save(purchase);

        Path path = Paths.get(
                environment.getProperty("user.dir"),
                "upload",
                "static"
        );
        try {
            image.transferTo(
                    path.resolve(saved.getId() + ".jpg").toFile()
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Purchase findById(int id) {
        return purchaseRepository.getOne(id);
    }

    @Override
    public void removeById(int id) {
        purchaseRepository.deleteById(id);
    }
}
