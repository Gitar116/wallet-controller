package ru.itpark.service;

import ru.itpark.entity.Balance;

import java.util.List;

public interface BalanceService {
    Balance findById(int id);

    List<Balance> getAll();

    void update(int balanceId, int money);

    void sellFrom(int balanceId, int price);

    void removePurch(int balanceId, int price);

    void saveBalance(Balance balance);

    void removeBalance(int id);
}
