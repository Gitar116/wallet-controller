package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.entity.Balance;

@Repository
public interface BalanceRepository extends JpaRepository<Balance, Integer> {
}
