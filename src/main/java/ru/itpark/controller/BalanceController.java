package ru.itpark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itpark.entity.Balance;
import ru.itpark.service.BalanceService;
import ru.itpark.service.BaseService;

@Controller
@RequestMapping("/balanceList")
public class BalanceController {
    private final BalanceService balanceService;
    private final BaseService baseService;

    public BalanceController(BalanceService balanceService, BaseService baseService) {
        this.balanceService = balanceService;
        this.baseService = baseService;
    }

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("balanceList", balanceService.getAll());
        return "pages/purchaseList";
    }

    @GetMapping("/{id}")
    public String getBalance(@PathVariable int id, Model model) {
        model.addAttribute("balance", balanceService.findById(id));
        return "pages/balance-update";
    }

    @GetMapping("/update")
    public String addFormUpdate() {
        return "pages/balance-update";
    }

    @PostMapping("/{id}/update")
    public String update(@PathVariable int id, Balance balance) {
        baseService.updateBalance(id, balance);
        return "redirect:/purchaseList";
    }

    @GetMapping("/add")
    public String addFormAdd() {
        return "/pages/balance-add.html";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute Balance balance) {
        balanceService.saveBalance(balance);
        return "redirect:/purchaseList";
    }

    @PostMapping("/{id}/remove")
    public String remove(@PathVariable int id) {
        balanceService.removeBalance(id);
        return "redirect:/purchaseList";
    }
}