package ru.itpark.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itpark.entity.Account;
import ru.itpark.entity.Purchase;
import ru.itpark.service.BalanceService;
import ru.itpark.service.BaseService;
import ru.itpark.service.PurchaseService;

@Controller
@RequestMapping("/purchaseList")
public class PurchaseController {
    private final PurchaseService purchaseService;
    private final BalanceService balanceService;
    private final BaseService baseService;

    public PurchaseController(PurchaseService purchaseService, BalanceService balanceService, BaseService baseService) {
        this.purchaseService = purchaseService;
        this.balanceService = balanceService;
        this.baseService = baseService;
    }

    @GetMapping
    public String getAll(Model model, @AuthenticationPrincipal Account account) {
        model.addAttribute("account", account);
        model.addAttribute("purchaseList", purchaseService.getAll());
        model.addAttribute("balanceList", balanceService.getAll());
        return "pages/purchaseList";
    }

    @GetMapping("/add")
    @PreAuthorize("hasRole('ADMIN')")
    public String addForm(Model model) {
        model.addAttribute("balanceList", balanceService.getAll());
        return "/pages/purchase-add";
    }

    @PostMapping("/add")
    @PreAuthorize("hasRole('ADMIN')")
    public String add(@ModelAttribute Purchase purchase, @RequestParam MultipartFile image) {
        baseService.savePurchase(purchase, image);
        return "redirect:/purchaseList";
    }

    @GetMapping("/{id}")
    public String get(@PathVariable int id, Model model) {
        model.addAttribute("purchase", purchaseService.findById(id));
        return "pages/purchase";
    }

    @PostMapping("/{id}/remove")
    @PreAuthorize("hasRole('ADMIN')")
    public String remove(@PathVariable int id) {
        baseService.removePurchase(id);
        return "redirect:/purchaseList";
    }
}

