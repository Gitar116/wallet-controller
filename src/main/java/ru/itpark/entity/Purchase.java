package ru.itpark.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;


@Entity
@Data
@AllArgsConstructor
public class Purchase {
    @Id
    @GeneratedValue
    private int id;
    @Column(nullable = false)
    private String category;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private int price;
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date created;

    @ManyToOne
    private Balance balance;

    public Purchase() {}

    public Purchase(String category, String description, int price, Date created) {
        this.category = category;
        this.description = description;
        this.price = price;
        this.created = created;
    }
}
