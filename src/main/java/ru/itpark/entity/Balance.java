package ru.itpark.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
public class Balance {
    @Id
    @GeneratedValue
    private int id;
    @Column(nullable = false)
    private int balance;
    @Column(nullable = false)
    private String type;

    public Balance() {}

    public Balance(int balance, String type) {
        this.balance = balance;
        this.type = type;
    }
}

