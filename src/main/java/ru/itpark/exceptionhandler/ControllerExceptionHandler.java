package ru.itpark.exceptionhandler;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.itpark.exception.BalanceNotFoundException;
import ru.itpark.exception.PurchaseNotFoundException;

@ControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler(BalanceNotFoundException.class)
    public String handleBalanceNotFound(
            Model model, BalanceNotFoundException e
    ) {
        model.addAttribute("message", e.getMessage());
        return "balance-not-found";
    }

    @ExceptionHandler(PurchaseNotFoundException.class)
    public String handlePurchaseNotFound(
            Model model, PurchaseNotFoundException e
    ) {
        model.addAttribute("message", e.getMessage());
        return "purchase-not-found";
    }
}
