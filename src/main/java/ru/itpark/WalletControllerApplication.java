package ru.itpark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itpark.entity.Account;
import ru.itpark.entity.Balance;
import ru.itpark.entity.Purchase;
import ru.itpark.repository.AccountRepository;
import ru.itpark.repository.BalanceRepository;
import ru.itpark.repository.PurchaseRepository;

import java.util.Date;
import java.util.List;

@SpringBootApplication
public class WalletControllerApplication {

    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(WalletControllerApplication.class, args);
        PurchaseRepository repository = context.getBean(PurchaseRepository.class);
        BalanceRepository balanceRepository = context.getBean(BalanceRepository.class);

        Balance cash = balanceRepository.save(new Balance(40000, "Cash"));
        Balance bank = balanceRepository.save(new Balance(100000, "Bank"));

        Purchase first = new Purchase("Home", "TV", 35000, new Date());
        first.setBalance(cash);
        repository.save(first);

        Purchase second = new Purchase("Car", "Oil", 1500, new Date());
        second.setBalance(bank);
        repository.save(second);

        Purchase third = new Purchase("Food", "Pizza", 500, new Date());
        third.setBalance(cash);
        repository.save(third);

        var encoder = context.getBean(PasswordEncoder.class);
        var repository2 = context.getBean(AccountRepository.class);
        repository2.saveAll(
                List.of(
                        new Account(0,
                                "admin",
                                encoder.encode("admin"),
                                List.of(new SimpleGrantedAuthority("ROLE_ADMIN")),
                                true,
                                true,
                                true,
                                true
                        ),
                        new Account(
                                0,
                                "user",
                                encoder.encode("user"),
                                List.of(new SimpleGrantedAuthority("ROLE_USER")),
                                true,
                                true,
                                true,
                                true
                        )
                )
        );
    }
}
